## Green Makeathon
- Many of the marine wildlife species that are protected under the wildlife protection act cannot be readily identified by the forest staff. Such an application can help as useful tool in crime control. 
 These protected marine species get caught accidently in fishing nets. Fishermen have to cut nets to release the protected / endangered species. Mangrove foundation has a scheme to compensate the fishermen for loss of their nets. A mobile app to lodge their claims, verify the genuineness of the claim and provide status would really help.

# Technology:
- We're going to use Spring boot along with Docker to deploy in Kubernetes Cluster.
- Kubernetes Cluster setup is ready using miniKube
- We'll be enabling Authentication for all the endpoints most possibly using OAuth - In Progress
